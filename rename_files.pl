#!/usr/bin/perl
# 批量对文件进行重命名

print "enter a dir name:";
my $dir_name = <STDIN>;
chomp $dir_name;
my @name_list = get_name_list($dir_name);

foreach my $file_name (@name_list) {
    $new_name = get_new_name($dir_name);
	rename "./$dir_name/$file_name", "./$dir_name/$new_name";
print "old name: $file_name    ";
print "new name: $new_name\n";
}
0;

sub get_new_name {
    my ($dir_name) = @_;
    my $name = "0000.jpg";
    my $i = 1;
    while (is_name_exist($name, $dir_name) ){
        $name = sprintf("%04d.jpg", $i);
        $i++;
    }
    return $name;
}

sub get_name_list {
    my $dir_name = shift;
	my @name_list;

	opendir DH, $dir_name or die "cannot open:$!\n";
	foreach my $file_name (readdir DH) {
		if ($file_name ne '.' && $file_name ne '..') {
			push @name_list, $file_name;
		}
	}
	closedir DH;

	return @name_list;
}

sub is_name_exist {
    my ($file_name, $dir_name) = @_;

    my @name_list = get_name_list($dir_name);
	foreach my $name (@name_list) {
		if ($file_name eq $name) {
			return 1;
		}
	}
	return 0;
}