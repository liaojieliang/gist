 // 用 strcoll 实现中文拼音排序。
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <locale.h>

int main (void) {
    int i;
    char str[10][4]= {"镕","堃","趙","錢","孫","李","周","吳","鄭","王"};

    printf ("按内码排序：");
    qsort(str, 10, 4, strcoll);
    for (i = 0; i < 10; i++)
        printf ("%s", str[i]);
    printf("\n");

    printf ("按音序排序：");
    setlocale (LC_ALL, "");
    qsort(str, 10, 4, strcoll);
    for (i = 0; i < 10; i++)
        printf ("%s", str[i]);
    printf("\n");

    return 0;
}
