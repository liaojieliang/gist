
# 求平均
cat data | awk '{sum+=$1} END {print "Average = ", sum/NR}'

# 求方差
 cat data | awk '{a[++i]=$1;} END{for(i in a)sum += a[i];ave=sum/NR;for(i in a) sigma += (a[i]-ave)*(a[i]-ave);print sigma/NR}'