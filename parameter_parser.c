// C C++ 命令行参数处理。
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <getopt.h> 
#include <libgen.h>

enum _run_mode {
    MODE_TEST,
    MODE_READ,
    MODE_WRITE,
    MODE_INVALID
};

typedef struct {
    uint32_t        address;
    uint32_t        size;
    enum _run_mode  mode;
    uint32_t        read_size;
    uint32_t        times;
    char           *in_file;
} Params;

const char * const VERSION = "1.0";

static const char short_opts [] = ":a:s:t:r:w:vh";
static const struct option long_opts [] = {
    { "address",    required_argument,      NULL, 'a' },
    { "size",       required_argument,      NULL, 's' },
    { "test",       required_argument,      NULL, 't' },
    { "read",       required_argument,      NULL, 'r' },
    { "write",      required_argument,      NULL, 'w' },
    { "version",    no_argument,            NULL, 'v' },
    { "help",       no_argument,            NULL, 'h' },
    { 0, 0, 0, 0 }
};

static void usage(char *prog_name) {
    printf("Usage: %s [options]\n"
            "Options:\n"
            "   -a | --address        Start address.\n"
            "   -s | --size           EMIF capacity size.\n"
            "   -t | --test           Testing mode, Loop times that read/write and check with the specified size data .\n"
            "   -r | --read           Read mode, read size bytes and print out.\n"
            "   -w | --write          Write mode, Write data to address from the specified file.\n"
            "   -v | --version\n"
            "   -h | --help\n"
            "\n"
            "e.g. :\n"
            "       ./%s -a 0x00 -s 65536 -t 10\n"
            "       ./%s -a 0x00 -s 65536 -r 64\n"
            "       ./%s -a 0x00 -s 65536 -w /home/root/test.bit\n"
            "\n"
            , prog_name, prog_name, prog_name, prog_name);
}

/* Parsing input parameters */
bool parse_parameter(Params *const params, int argc, char **argv) {
    int c = 0;
    while ((c = getopt_long(argc, argv, short_opts, long_opts, NULL))!= -1) {
        switch (c) {
        case 'a':
            params->address = strtoul(optarg, 0, 16);
            break;
        case 's':
            params->size = strtoul(optarg, 0, 10);
            break;
        case 't':
            params->mode = MODE_TEST;
            params->times = atoi(optarg);
            break;
        case 'r':
            params->mode = MODE_READ;
            params->read_size = strtoul(optarg, 0, 10);
            break;
        case 'w':
            params->mode = MODE_WRITE;
            params->in_file = optarg;
            break;
        case 'v':
            printf("version : %s\n", VERSION);
            exit(0);
        case 'h':
            usage(basename(argv[0]));
            exit(0);
            break;
        default :
            printf("Try %s --help for more information\n", argv[0]);
            exit(-1);
        }
    }

    // Check parameter requirements.
    if ((params->address == 0 || params->size == 0)
        || (params->mode == MODE_TEST && params->times <= 0)
        || (params->read_size > params->size)) {
        printf("Try %s --help for more information\n", argv[0]);
        return false;
    }

    return true;
}